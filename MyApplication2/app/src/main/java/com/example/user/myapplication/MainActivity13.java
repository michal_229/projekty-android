package com.example.user.myapplication;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;


public class MainActivity13 extends Activity {

    EditText editTextInput;
    Button buttonWyslij;
    EditText editTextOutput;

    String displayText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity13);

        editTextInput = (EditText) findViewById(R.id.editTextInput);
        buttonWyslij = (Button) findViewById(R.id.buttonWyslij);
        editTextOutput = (EditText) findViewById(R.id.editTextOutput);


        buttonWyslij.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextInput.getText().length() != 0 && editTextInput.getText().toString() != "") {
                    displayText = editTextInput.getText().toString();
                    AsyncTaskWebService task = new AsyncTaskWebService();
                    task.execute();
                } else {
                    editTextOutput.setText("Please enter name");
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity13, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }












    private class AsyncTaskWebService extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) { //Invoke webservice
            try {
                displayText = WebServiceConn.invokeHelloWorldWS(editTextInput.getText().toString(),"getPhoto");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            editTextOutput.setText(displayText);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }
}
