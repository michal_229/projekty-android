package com.example.user.myapplication;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by user on 18.06.15.
 */
public class SoapRequest {

    private static String NAMESPACE = "http://package_1337/";
    private static String URL = "http://192.168.0.17:8080/TimWebAppAndroid/GetDataService?WSDL";
    private static String SOAP_ACTION = "http://package_1337/";

    private static String METHOD_NAME = "getPhoto";


    //private static final String SOAP_ACTION = "xxx";
    //private static final String METHOD_NAME = "xxx";
    //private static final String NAMESPACE = "xxx";
    //private static final String URL = "url of the webservice";

    public static SoapObject soap() throws IOException, XmlPullParserException {
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

/* Here you can add properties to your requests */
        PropertyInfo pi1 = new PropertyInfo();
        pi1.name = "xxx";
        pi1.type = String.class;
        request.addProperty(pi1.toString(), "xxx");

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;
        androidHttpTransport.call(SOAP_ACTION, envelope);
        SoapObject soapResult = (SoapObject) envelope.bodyIn;
        return soapResult;
    }

}
