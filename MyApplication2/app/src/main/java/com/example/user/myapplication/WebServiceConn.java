package com.example.user.myapplication;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by user on 18.06.15.
 */
public class WebServiceConn {

    //Namespace of the Webservice - can be found in WSDL
    private static String NAMESPACE = "http://package_1337/";
    //Webservice URL - WSDL File location
    private static String URL = "http://192.168.0.17:8080/TimWebAppAndroid/GetDataService?WSDL";
    //SOAP Action URI again Namespace + Web method name
    private static String SOAP_ACTION = "http://package_1337/";

    public static String invokeHelloWorldWS(String name, String webMethName) throws IOException {
        String resTxt = null;
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters
        PropertyInfo sayHelloPI = new PropertyInfo();
        // Set Name
        sayHelloPI.setName("photo");
        // Set Val

        sayHelloPI.setValue(name);

        // Set dataType
        sayHelloPI.setType(String.class);
        // Add the property to request object
        request.addProperty(sayHelloPI);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        //new MarshalDouble().register(envelope);
        new MarshalBase64().register(envelope);   //serialization
        envelope.encodingStyle = SoapEnvelope.ENC;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;
    }

    public static byte[] convertDocToByteArray(String sourcePath) throws IOException {
        File f = new File(sourcePath);
        long l = f.length();
        byte[] buf = new byte[(int) l];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            InputStream fis = new FileInputStream(sourcePath);

            for (int readNum; (readNum = fis.read(buf)) != -1; ) {
                bos.write(buf, 0, readNum);
                Log.i("", "read num bytes: " + readNum);
            }
        } catch (IOException e) {
            System.out.println("IO Ex" + e);
        }
        byte[] bytes = bos.toByteArray();

//	            for(int i = 0;i<bytes.length;i++)
//	            {
//	            	Log.i("","bytes : "+bytes[i]);
//	            }

        return bytes;
    }

}
