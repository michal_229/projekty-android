package com.example.user.myapplication;

import android.hardware.Camera;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity7 extends ActionBarActivity {
    Button button1, button2, button3;
    EditText editText;
    Camera camera;
    CameraPreview mCameraPreview;
    FrameLayout preview;
    MainActivity7 context = this;
    String str2hex[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity7);

        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);

        editText = (EditText) findViewById(R.id.editText);




        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int camNum = Camera.getNumberOfCameras();
                for (int i = 0; i < camNum && camera == null; i++) {
                    try {
                        camera = Camera.open(i);
                        log("otwarto kamere " + i);
                        if (camera == null) {
                            log("camera == null");
                        }
                        else {
                            log("camera != null");

                        }
                    }
                    catch (Exception e) {
                        log("nie udalo sie otworzyc kamery " + i);
                        log(e.getMessage());
                        log(e.toString());
                    }

                    try {
                        mCameraPreview = new CameraPreview(context, camera);
                        preview = (FrameLayout) findViewById(R.id.frameLayout);
                        preview.addView(mCameraPreview);
                        log("preview ok");
                    }
                    catch (Exception e) {
                        log("preview nie ok");
                        log(e.getMessage());
                    }
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    camera.setPreviewDisplay(mCameraPreview.mSurfaceHolder);
                    camera.startPreview();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    camera.takePicture(null, null, new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(byte[] data, Camera camera) {

                            log("zrobiono zdjecie");
                            log("zdjecie zajmuje " + data.length);
                            saveImage(data);
                            /*for (int i = 0; i < 256; i++) {
                                if (i % 10 == 0) {
                                    log("");
                                }
                                int bajt = (int) data[i];
                                if (bajt < 0) bajt = (-1)*bajt;
                                log(str2hex[bajt/16] + str2hex[bajt % 16] + " ", false);
                            }*/
                        }
                    });
                }
                catch (Exception e) {
                    log("nie zrobiono zdjecia");
                    log(e.getMessage());
                    log(e.toString());
                }
               /* */

            }
        });
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            log("!mediaStorageDir.exists()");
            if (!mediaStorageDir.mkdirs()) {
                log("!mediaStorageDir.mkdirs()");
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("0_yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private void saveImage(byte[] data) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            log("pictureFile == null, return");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
            log("zapisano zdjecie");
        } catch (FileNotFoundException e) {
            log("FileNotFoundException");
        } catch (IOException e) {
            log("IOException");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity7, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void log(String s) {
        editText.append(">> " + s + "\n");
    }

    public void log(String s, boolean newLine) {
        if (newLine)
            log(s);
        else
            editText.append(s);
    }
}
