package com.example.user.mycamapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.EditText;

import java.io.IOException;

/**
 * Created by user on 22.06.15.
 */
public class AsyncTaskWebService extends AsyncTask<String, Void, Void> {
    String method;
    String message;
    String odebranyTekst;
    EditText displayText;
    Activity activity1 = null;

    AsyncTaskWebService(String metoda, String wiadomosc, EditText et, Activity a) {
        method = metoda;
        message = wiadomosc;
        displayText = et;

        activity1 = a;
        this.execute();
    }

    AsyncTaskWebService(String metoda, String wiadomosc, EditText et) {
        method = metoda;
        message = wiadomosc;
        displayText = et;
        this.execute();
    }

    @Override
    protected Void doInBackground(String... params) { //Invoke webservice
        try {
            odebranyTekst = WebServiceConn.invokeHelloWorldWS(method, message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        displayText.append(odebranyTekst + "\n");
        if (odebranyTekst.contains("zalogowan") && activity1 != null) activity1.finish();
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}
