package com.example.user.mycamapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NowyProjektActivity extends Activity {
    EditText editTextNazwaProjektu, editTextOdstep, editTextIloscZdjec;
    Button buttonAnulujProjekt, buttonOkProjekt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nowy_projekt);

        buttonOkProjekt = (Button) findViewById(R.id.buttonOkProjekt);
        buttonAnulujProjekt = (Button) findViewById(R.id.buttonAnulujProjekt);

        editTextNazwaProjektu = (EditText) findViewById(R.id.editTextNazwaProjektu);
        editTextOdstep = (EditText) findViewById(R.id.editTextOdstep);
        editTextIloscZdjec = (EditText) findViewById(R.id.editTextIloscZdjec);

        buttonOkProjekt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences projectSettings = getSharedPreferences("projectSettings", 0);
                SharedPreferences.Editor projSettEditor = projectSettings.edit();
                projSettEditor.putString("nazwa_projektu", editTextNazwaProjektu.getText().toString());
                projSettEditor.putString("odstep", editTextOdstep.getText().toString());
                projSettEditor.putString("ilosc_zdjec", editTextIloscZdjec.getText().toString());
                projSettEditor.commit();


                startActivity(new Intent(NowyProjektActivity.this, ProjektActivity.class));
                finish();
            }
        });

        buttonAnulujProjekt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nowy_projekt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
