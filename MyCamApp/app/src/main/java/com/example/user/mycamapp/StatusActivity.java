package com.example.user.mycamapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class StatusActivity extends Activity {
    Button buttonStatusCofnij, buttonStatusOdswiez;
    EditText editTextStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        buttonStatusCofnij = (Button) findViewById(R.id.buttonStatusCofnij);
        buttonStatusOdswiez = (Button) findViewById(R.id.buttonStatusOdswiez);
        editTextStatus = (EditText) findViewById(R.id.editTextStatus);


        new AsyncTaskWebService("listaProjektow", " ", editTextStatus);

        buttonStatusCofnij.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        buttonStatusOdswiez.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextStatus.setText("");
                new AsyncTaskWebService("listaProjektow", " ", editTextStatus);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
