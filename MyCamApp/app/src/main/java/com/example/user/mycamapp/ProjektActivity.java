package com.example.user.mycamapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


public class ProjektActivity extends Activity {
    Button button, button2, button3;
    EditText editText;
    FrameLayout frameLayout;

    Camera mCamera;
    CameraPreview mCameraPreview;

    String displayText, obrazekDoWyslania = "tekst do nadpisania";
    String nazwaProjektu;
    int odstep;
    int iloscZrobionychZdjec;
    int iloscZdjec;

    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projekt);

        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        editText = (EditText) findViewById(R.id.editText);
        frameLayout = (FrameLayout) findViewById(R.id.camera_preview);

        mCamera = getCameraInstance();
        Camera.Parameters parametry = mCamera.getParameters();
        parametry.setPictureSize(2048, 1536);
        mCamera.setParameters(parametry);

        mCameraPreview = new CameraPreview(this, mCamera);
        frameLayout.addView(mCameraPreview);

        mCamera.startPreview();

        SharedPreferences projectSettings = getSharedPreferences("projectSettings", 0);
        try {
            nazwaProjektu = projectSettings.getString("nazwa_projektu", null);
            new AsyncTaskWebService("setActiveProject", nazwaProjektu, editText);
        } catch (Exception e) {

        }

        try {
            odstep = Math.max(Integer.parseInt(projectSettings.getString("odstep", null)), 1);
        } catch (Exception e) {
            odstep = 1;
        }

        try {
            iloscZdjec = Math.max(Integer.parseInt(projectSettings.getString("ilosc_zdjec", null)), 3);
        } catch (Exception e) {
            iloscZdjec = 10;
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mCamera.takePicture(null, null, mPicture);
                        iloscZrobionychZdjec++;
                        if (iloscZrobionychZdjec > iloscZdjec) {
                            timer.cancel();
                            new AsyncTaskWebService("startPrzetwarzania", "stab_avg", editText);
                            finish();
                        }
                    }
                }, 0, odstep * 1000);
                log("start timera, czekamy");
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer == null)
                    log("nie uruchomiono serii!");
                else
                    timer.cancel();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer != null) timer.cancel();
                new AsyncTaskWebService("startPrzetwarzania", "stab_avg", editText);
                finish();
            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }




    public void log(String s) {
        editText.append(s + "\n");
    }




    private Camera getCameraInstance() {
        Camera camera = null;
        int i = Camera.getNumberOfCameras();
        log("istnieje " + i + " kamer");
        for (int j = 0; j < i && camera == null; j++) {
            try {
                camera = Camera.open();
                if (camera == null)
                    log("brak kamery");
                else
                    log("znaleziono kamere");
            } catch (Exception e) {
                log("exception: nie mozna znalezc kamery " + j + "!");
            }
        }
        return camera;
    }




    Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes, Camera camera) {
            log("zrobiono zdjecie");
            obrazekDoWyslania = Base64.encodeToString(bytes, Base64.DEFAULT);
            log("zapisano zdjecie w base64");
            mCamera.startPreview();
            new AsyncTaskWebService("getPhoto", obrazekDoWyslania, editText);
        }
    };





}
