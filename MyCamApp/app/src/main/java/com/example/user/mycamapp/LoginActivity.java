package com.example.user.mycamapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class LoginActivity extends Activity {

    Button buttonZaloguj, buttonAnuluj, buttonZarejestruj;
    EditText editTextLogin, editTextHaslo, editTextLoginInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buttonZaloguj = (Button) findViewById(R.id.buttonLoginOk);
        buttonAnuluj = (Button) findViewById(R.id.buttonLoginAnuluj);
        buttonZarejestruj = (Button) findViewById(R.id.buttonLoginZarejestruj);
        editTextLogin = (EditText) findViewById(R.id.editTextLogin);
        editTextHaslo = (EditText) findViewById(R.id.editTextHaslo);
        editTextLoginInfo = (EditText) findViewById(R.id.editTextLoginInfo);

        new AsyncTaskWebService("wyloguj", " ", editTextLoginInfo);

        buttonZaloguj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = editTextLogin.getText().toString();
                String haslo = editTextHaslo.getText().toString();

                new AsyncTaskWebService("login", login+";"+haslo, editTextLoginInfo, LoginActivity.this);
            }
        });

        buttonZarejestruj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = editTextLogin.getText().toString();
                String haslo = editTextHaslo.getText().toString();

                new AsyncTaskWebService("rejestracja", login+";"+haslo, editTextLoginInfo);
            }
        });

        buttonAnuluj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
