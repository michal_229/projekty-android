package com.example.user.mycamapp;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by user on 18.06.15.
 */
public class WebServiceConn {
    private static String NAMESPACE = "http://package_1337/";
    private static String URL = "http://192.168.0.10:8080/TimWebAppAndroid/GetDataService?WSDL";
    private static String SOAP_ACTION = "http://package_1337/";

    public static String invokeHelloWorldWS(String nazwaMetody, String wiadomoscDoWyslania) throws IOException {
        String resTxt = null;
        SoapObject request = new SoapObject(NAMESPACE, nazwaMetody);
        PropertyInfo sayHelloPI = new PropertyInfo();
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        new MarshalBase64().register(envelope);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        sayHelloPI.setName("wiadomosc");
        sayHelloPI.setValue(wiadomoscDoWyslania);
        sayHelloPI.setType(String.class);
        request.addProperty(sayHelloPI);

        envelope.encodingStyle = SoapEnvelope.ENC;
        envelope.setOutputSoapObject(request);

        try {
            androidHttpTransport.call("\"" + SOAP_ACTION + nazwaMetody + "\"", envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            resTxt = response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            resTxt = "Error occured";
        }
        return resTxt;
    }
}
